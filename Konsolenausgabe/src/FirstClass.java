
public class FirstClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("Aufgabe 1");
		
		System.out.print("\n");
		System.out.print("Das ist ein Beispielsatz.\n");
		System.out.print("\n");
		System.out.println("Ein Beispielsatz ist das.\n");
		
		int alter = 23;
		String name = "Jannik";
		System.out.println("Ich hei�e " + name + " und bin " + alter + " Jahre alt.\n");
		System.out.print("\n");
		
		System.out.println("Aufagbe 2 \n");
		
		System.out.println("             *");
		System.out.println("            ***");
		System.out.println("           *****");
		System.out.println("          *******");
		System.out.println("         *********");
		System.out.println("        ***********");
		System.out.println("       *************");
		System.out.println("      ***************");
		System.out.println("     *****************");
		System.out.println("           *****");
		System.out.println("           *****");
		System.out.println("           *****");
		System.out.println("\n");
		
		System.out.println("Aufgabe 3 \n");
		
		String s = "Tabelle";
		System.out.printf(" \n|%s| \n", s);
		
		double d = 22.4234234;
		System.out.printf( "|%11.2f| |%11.2f|\n" , d, -d);
		
		
		double a = 111.2222;
		System.out.printf( "|%11.2f| |%11.2f|\n" , a, -a);
		
		
		double b = 4.0;
		System.out.printf( "|%11.2f| |%11.2f|\n" , b, -b);
		
		
		double c = 1000000.551;
		System.out.printf( "|%11.2f| |%11.2f|\n" , c, -c);
		
		
		double e = 97.34;
		System.out.printf("|%11.2f| |%11.2f|\n", e, -e);
		
		
		System.out.println("\n");
		System.out.println("ENDE.");

	
	}

}
